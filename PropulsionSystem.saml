
// October 2012: With S3E v1.8, trying to check this model results in the error below.
// November 2012: Still happening with v1.9.
//
// ERROR - Failed to initalisate prism interace
// de.ovgu.cse.saml.helper.SamlException: CouldNotTranslate: There were Errors during the convertion

component main
	
	constant int open := 0;
	constant int closed := 1;
	constant int timing := 2;
	
	constant int K6TimeOut := 20;
	constant int hazardTimeOut := 23;
	
	constant double probFailSwitch := 2E-5;
	constant double probFailRelay := 3E-3;
	constant double probFailValve := 2E-4;
	
	
	// *** Demand formulas ***
	// demand to react := demand to open or demand to close
	
	formula demandK1 := ((armingCircuit & K1.state = open)   | (!armingCircuit & K1.state = closed));
	formula demandK2 := ((armingCircuitK2 & K2.state = closed) | (!armingCircuitK2 & K2.state = open));
	formula demandK3 := ((armingCircuit & K3.state = open)   | (!armingCircuit & K3.state = closed));
	formula demandK4 := ((firingCircuit & K4.state = open)   | (!firingCircuit & K4.state = closed));
	formula demandK5 := ((firingCircuit & K5.state = open)   | (!firingCircuit & K5.state = closed));
	formula demandK6 := (demandK6Open | demandK6Close);
	
	formula demandK6Open := (thrustingCircuit & K6.state = timing & K6.timer = K6TimeOut);
	formula demandK6Close := (!thrustingCircuit & (K6.state = open | K6.state = timing));
	
	formula demandIV1 := ((armingCircuit & IV1.state = closed)   | (!armingCircuit & IV1.state = open));
	formula demandIV2 := ((armingCircuit & IV2.state = closed) | (!armingCircuit & IV2.state = open));
	formula demandIV3 := ((thrustingCircuit & IV3.state = closed) | (!thrustingCircuit & IV3.state = open));
	
	
	// *** Circuit formulas ***
	// true if circuits are closed
		
	formula armingCircuit := ((S1.state = closed | K1.state = closed) & S3.state = closed & (K6.state = closed | K6.state = timing));
	formula armingCircuitK2 := (S1.state = closed & S3.state = closed & (K6.state = closed | K6.state = timing));
	
	formula firingCircuit := ((S2.state = closed | K4.state = closed) & K2.state = closed & K3.state = closed);
	formula thrustingCircuit := (K5.state = closed);

	formula thrusting := (IV2.state = open & IV3.state = open);

	
	//  *** Switch Failures ***
	// (pseudo-) per-demand, transient failure
	// (pseudo-) per-demand behavior is given implicitly by non-determinism of the switch components S1-S3
	
	component failureS1
		fails : [0..1] init 0;
			
		true -> choice: (probFailSwitch:(fails' = 1) + (1 - probFailSwitch):(fails' = 0));	
	endcomponent

	component failureS2
		fails : [0..1] init 0;
			
		true -> choice: (probFailSwitch:(fails' = 1) + (1 - probFailSwitch):(fails' = 0));	
	endcomponent
	
	component failureS3
		fails : [0..1] init 0;
			
		true -> choice: (probFailSwitch:(fails' = 1) + (1 - probFailSwitch):(fails' = 0));	
	endcomponent

	
	// *** Relay Failures ***
	// per-demand, transient failures	
	
	component failureK1
		fails : [-1..1] init -1;
		
		 demandK1 | fails = -1 -> choice: (probFailRelay:(fails' = 1) + (1-probFailRelay):(fails' = 0));
		!demandK1 & fails > -1 -> choice: (1:(fails' = fails));
	endcomponent
	
	component failureK2
		fails : [-1..1] init -1;
		
		 demandK2 | fails = -1 -> choice: (probFailRelay:(fails' = 1) + (1-probFailRelay):(fails' = 0));
		!demandK2 & fails > -1 -> choice: (1:(fails' = fails));
	endcomponent
	
	component failureK3
		fails : [-1..1] init -1;
		
		 demandK3 | fails = -1 -> choice: (probFailRelay:(fails' = 1) + (1-probFailRelay):(fails' = 0));
		!demandK3 & fails > -1 -> choice: (1:(fails' = fails));
	endcomponent
	
	component failureK4
		fails : [-1..1] init -1;
		
		 demandK4 | fails = -1 -> choice: (probFailRelay:(fails' = 1) + (1-probFailRelay):(fails' = 0));
		!demandK4 & fails > -1 -> choice: (1:(fails' = fails));
	endcomponent
	
	component failureK5
		fails : [-1..1] init -1;
		
		 demandK5 | fails = -1 -> choice: (probFailRelay:(fails' = 1) + (1-probFailRelay):(fails' = 0));
		!demandK5 & fails > -1 -> choice: (1:(fails' = fails));
	endcomponent
	
	// per-demand failure only for opening, not for closing or timing
	component failureK6
		fails : [-1..1] init -1;
		
		 demandK6Open | fails = -1 -> choice: (probFailRelay:(fails' = 1) + (1-probFailRelay):(fails' = 0));
		!demandK6Open & fails > -1 -> choice: (1:(fails' = fails));
	endcomponent
	
	
	// *** Valve Failures ***
	// per-demand, transient failures
		
	component failureIV1
		fails : [-1..1] init -1;
		
		 demandIV1 | fails = -1 -> choice: (probFailValve:(fails' = 1) + (1-probFailValve):(fails' = 0));
		!demandIV1 & fails > -1 -> choice: (1:(fails' = fails));
	endcomponent
	
	component failureIV2
		fails : [-1..1] init -1;
		
		 demandIV2 | fails = -1 -> choice: (probFailValve:(fails' = 1) + (1-probFailValve):(fails' = 0));
		!demandIV2 & fails > -1 -> choice: (1:(fails' = fails));
	endcomponent
	
	component failureIV3
		fails : [-1..1] init -1;
		
		 demandIV3 | fails = -1 -> choice: (probFailValve:(fails' = 1) + (1-probFailValve):(fails' = 0));
		!demandIV3 & fails > -1 -> choice: (1:(fails' = fails));
	endcomponent
	
		
	// *** Switches ***

	component S1
		// open, closed 
		state : [0..1] init 0;
		
		failureS1.fails = 1 -> choice: (1:(state' = state));
		failureS1.fails = 0 -> choice: (1:(state' = 0)) + choice: (1:(state' = 1));
	endcomponent


	component S2
		state : [0..1] init 0;
		
		failureS2.fails = 1 -> choice: (1:(state' = 0));
		failureS2.fails = 0 -> choice: (1:(state' = 0)) + choice: (1:(state' = 1));
	endcomponent
	
	
	component S3
		state : [0..1] init 1;
		
		failureS3.fails = 1 -> choice: (1:(state' = 1));
		failureS3.fails = 0 -> choice: (1:(state' = 0)) + choice: (1:(state' = 1));
	endcomponent
	
	
	//*** Relays ***
	
	component K1
		// open, closed
		state : [0..1] init 0;

		!demandK1 | (demandK1 & failureK1.fails = 1) -> choice: (1:(state' = state));
		
		// Note: 'fails < 1' as non-failure states for per-demands failure modes are -1 and 0 
		demandK1 &  armingCircuit & failureK1.fails < 1 -> choice: (1:(state' = closed));  
		demandK1 & !armingCircuit & failureK1.fails < 1 -> choice: (1:(state' = open));
	endcomponent
	
	component K2
		state : [0..1] init 1;
			
		!demandK2 | (demandK2 & failureK2.fails = 1) -> choice: (1:(state' = state));
		
		demandK2 &  armingCircuitK2 & failureK2.fails < 1 -> choice: (1:(state' = open));  
		demandK2 & !armingCircuitK2 & failureK2.fails < 1 -> choice: (1:(state' = closed));
	endcomponent
	
	component K3
		state : [0..1] init 0;
			
		!demandK3 | (demandK3 & failureK3.fails = 1) -> choice: (1:(state' = state));
		
		demandK3 &  armingCircuit & failureK3.fails < 1 -> choice: (1:(state' = closed));  
		demandK3 & !armingCircuit & failureK3.fails < 1 -> choice: (1:(state' = open));
	endcomponent
	
	component K4
		state : [0..1] init 0;
			
		!demandK4 | (demandK4 & failureK4.fails = 1) -> choice: (1:(state' = state));
		
		demandK4 &  firingCircuit & failureK4.fails < 1 -> choice: (1:(state' = closed));  
		demandK4 & !firingCircuit & failureK4.fails < 1 -> choice: (1:(state' = open));
	endcomponent
	
	component K5
		state : [0..1] init 0;
			
		!demandK5 | (demandK5 & failureK5.fails = 1) -> choice: (1:(state' = state));
		
		demandK5 &  firingCircuit & failureK5.fails < 1 -> choice: (1:(state' = closed));  
		demandK5 & !firingCircuit & failureK5.fails < 1 -> choice: (1:(state' = open));
	endcomponent
	
	component K6
		// open, closed, timing
		state : [0..2] init 1;
		timer : [0..20] init 0;	

		!thrustingCircuit -> choice: (1:(state' = closed) & (timer' = 0)); // close or stay closed
		
		thrustingCircuit & state = closed -> choice: (1:(state' = timing) & (timer' = 0)); // start timing
		thrustingCircuit & state = timing & timer < K6TimeOut -> choice: (1:(state' = timing) & (timer' = timer + 1)); // timing
		demandK6Open & failureK6.fails < 1 -> choice: (1:(state' = open) & (timer' = timer)); // end timing, open		 		  
		demandK6Open & failureK6.fails = 1 -> choice: (1:(state' = closed) & (timer' = 0)); // end timing, fail to open
		
		thrustingCircuit & state = open -> choice: (1: (state' = open) & (timer' = timer)); // stay opened	
	endcomponent
	
	
	// *** Valves ***
	
	component IV1
		state : [0..1] init 1;
			
		!demandIV1 | (demandIV1 & failureIV1.fails = 1) -> choice: (1:(state' = state));
		
		demandIV1 &  armingCircuit & failureIV1.fails < 1 -> choice: (1:(state' = open));  
		demandIV1 & !armingCircuit & failureIV1.fails < 1 -> choice: (1:(state' = closed));
	endcomponent	
	
	component IV2
		state : [0..1] init 1;
		
		!demandIV2 | (demandIV2 & failureIV2.fails = 1) -> choice: (1:(state' = state));
		
		demandIV2 &  armingCircuit & failureIV2.fails < 1 -> choice: (1:(state' = open));  
		demandIV2 & !armingCircuit & failureIV2.fails < 1 -> choice: (1:(state' = closed));
	endcomponent
	
	component IV3
		state : [0..1] init 1;
		
		!demandIV3 | (demandIV3 & failureIV3.fails = 1) -> choice: (1:(state' = state));
		
		demandIV3 &  thrustingCircuit & failureIV3.fails < 1 -> choice: (1:(state' = open));  
		demandIV3 & !thrustingCircuit & failureIV3.fails < 1 -> choice: (1:(state' = closed));
	endcomponent
	
	
	// *** Hazard Observer ***
	// Not part of the functional model itself
	// Checks if the system thrusts too long (i.e. 22 time units at a time)
	
	component hazardObserver
		counter : [0..23] init 0;
		
		!thrusting -> 1:(counter' = 0);
		
		thrusting & counter < hazardTimeOut -> choice: (1:(counter' = counter + 1));
		thrusting & counter = hazardTimeOut -> choice: (1:(counter' = counter));
	endcomponent
	
endcomponent

HAZARD main.hazardObserver.counter = main.hazardTimeOut;